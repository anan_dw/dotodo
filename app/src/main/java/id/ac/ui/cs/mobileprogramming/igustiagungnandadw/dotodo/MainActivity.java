package id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.db.TodoDatabase;
import id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.fragment.TodoListFragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {

    private FloatingActionButton newTodoBtn;
    private FloatingActionButton clrTodoBtn;
    private TodoDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = Room.databaseBuilder(getApplicationContext(), TodoDatabase.class, "todo_table").allowMainThreadQueries().addCallback(new RoomDatabase.Callback() {
            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                super.onCreate(db);
            }
        }).build();

        newTodoBtn = (FloatingActionButton) findViewById(R.id.btn_new_task);
        newTodoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchTodoCreationActivity();
            }
        });

        clrTodoBtn = (FloatingActionButton) findViewById(R.id.btn_clear_done_task);
        clrTodoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.todoDao().deleteDone();
                recreate();
            }
        });

    }

    @Override
    protected void onResume(){
        super.onResume();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        TodoListFragment todoListFragment  = new TodoListFragment();

        fragmentTransaction.add(R.id.container, todoListFragment);
        fragmentTransaction.commit();

    }

    private void launchTodoCreationActivity() {
        Intent intent = new Intent(this, NewTodoActivity.class);
        startActivity(intent);
    }

}
