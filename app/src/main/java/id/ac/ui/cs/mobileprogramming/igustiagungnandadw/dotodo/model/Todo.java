package id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.model;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "todo_table")
public class Todo {

    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "name")
    private String name;

    @Nullable
    @ColumnInfo(name = "due")
    private String due;

    @ColumnInfo(name = "is_done")
    private boolean isDone;

    public Todo(String name, String due) {
        this.name = name;
        this.due = due;
        this.isDone = false;
    }

    @Nullable
    public String getDue() {
        return due;
    }

    public void setDue(@Nullable String due) {
        this.due = due;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDone() {
        return this.isDone;
    }

    public void setDone(boolean status) {
        this.isDone = status;
    }

}
