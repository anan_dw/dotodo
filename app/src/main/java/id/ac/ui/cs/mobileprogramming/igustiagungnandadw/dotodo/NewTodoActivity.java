package id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.db.TodoDatabase;
import id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.model.Todo;

public class NewTodoActivity extends AppCompatActivity {

    private FloatingActionButton createTodoBtn;
    private EditText dueEt, nameEt;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_todo);

        TodoDatabase db = Room.databaseBuilder(getApplicationContext(), TodoDatabase.class, "todo_table").allowMainThreadQueries().addCallback(new RoomDatabase.Callback() {
            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                super.onCreate(db);
            }
        }).build();

        nameEt = (EditText) findViewById(R.id.et_task_name);

        createTodoBtn = (FloatingActionButton) findViewById(R.id.btn_create_task);
        createTodoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String taskName = nameEt.getText().toString();
                String taskDue = dueEt.getText().toString();
                if (!taskName.isEmpty()){
                    Todo newTodo = new Todo(taskName, taskDue);
                    db.todoDao().insert(newTodo);
                }
                finish();
            }
        });

        dueEt = (EditText) findViewById(R.id.et_task_due);
        setUpDateTimePicker();
        dueEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });

    }

    public void setUpDateTimePicker(){
        final Calendar calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        mHour = calendar.get(Calendar.HOUR_OF_DAY);
        mMinute = calendar.get(Calendar.MINUTE);
        datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        dueEt.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        timePickerDialog.show();
                    }
                }, mYear, mMonth, mDay);

        timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        dueEt.setText(dueEt.getText().toString() + " " + hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, false);
    }
}
