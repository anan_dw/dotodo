package id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.R;
import id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.db.TodoDatabase;
import id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.model.Todo;

import androidx.annotation.NonNull;

public class TodoAdapter extends ArrayAdapter {
    private final Activity context;
    private final List<Todo> itemArray;
    private TodoDatabase db;

    public TodoAdapter(@NonNull Activity context, List<Todo> todoArray, TodoDatabase db) {
        super(context, R.layout.listview_row, todoArray);

        this.db = db;
        this.context = context;
        this.itemArray = todoArray;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.listview_row, null,true);

        TextView nameTextField = (TextView) rowView.findViewById(R.id.todo_name);
        TextView dueTextField = (TextView) rowView.findViewById(R.id.todo_due);
        CheckBox check = (CheckBox) rowView.findViewById(R.id.todo_check);

        nameTextField.setText(itemArray.get(position).getName());
        dueTextField.setText(itemArray.get(position).getDue());
        check.setChecked(itemArray.get(position).isDone());


        check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                Todo updated = itemArray.get(position);
                updated.setDone(isChecked);
                db.todoDao().update(updated);
            }
        });
        return rowView;
    }   
}
