package id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.dao.TodoDao;
import id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.model.Todo;

@Database(entities = {Todo.class}, version = 1, exportSchema = false)
public abstract class TodoDatabase extends RoomDatabase {
    public abstract TodoDao todoDao();
}
