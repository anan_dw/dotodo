package id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.dao;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.model.Todo;

@Dao
public interface TodoDao {

    @Insert
    void insert(Todo todo);

    @Update
    void update(Todo todo);

    @Query("DELETE FROM todo_table WHERE is_done = 1")
    void deleteDone();

    @Query("SELECT * FROM todo_table")
    List<Todo> getAll();
}
