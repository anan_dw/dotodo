package id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.NewTodoActivity;
import id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.R;
import id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.adapter.TodoAdapter;
import id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.db.TodoDatabase;
import id.ac.ui.cs.mobileprogramming.igustiagungnandadw.dotodo.model.Todo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class TodoListFragment extends Fragment {

    private TodoDatabase db;
    private TextView noTodo;
    private ListView listOfTodo;
    private List<Todo> todos;

    public TodoListFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_list_fragment, container, false);
        db = Room.databaseBuilder(getActivity().getApplicationContext(), TodoDatabase.class, "todo_table").allowMainThreadQueries().addCallback(new RoomDatabase.Callback() {
            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                super.onCreate(db);
            }
        }).build();
        listOfTodo = (ListView) rootView.findViewById(R.id.lv_todo_list);
        noTodo = (TextView) rootView.findViewById(R.id.tv_no_todo);

        todos = db.todoDao().getAll();

        noTodo.setVisibility(todos.isEmpty() ? View.VISIBLE : View.INVISIBLE);

        TodoAdapter adapter = new TodoAdapter(getActivity(), todos, db);

        listOfTodo.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        todos = db.todoDao().getAll();
        noTodo.setVisibility(todos.isEmpty() ? View.VISIBLE : View.INVISIBLE);
    }



}

